﻿namespace Task19
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.DatabaseDataGridView = new System.Windows.Forms.DataGridView();
            this.NameTextBox = new System.Windows.Forms.TextBox();
            this.RefreshButton = new System.Windows.Forms.Button();
            this.DeleteButton = new System.Windows.Forms.Button();
            this.AddButton = new System.Windows.Forms.Button();
            this.NameLabel = new System.Windows.Forms.Label();
            this.SupervisorNamesListBox = new System.Windows.Forms.ListBox();
            this.CreateStudentLabel = new System.Windows.Forms.Label();
            this.SupervisorsInfoLabel = new System.Windows.Forms.Label();
            this.SupervisorNamesLabel = new System.Windows.Forms.Label();
            this.StudentsDataGridView = new System.Windows.Forms.DataGridView();
            this.StudentsLabel = new System.Windows.Forms.Label();
            this.SerializeToJSONButton = new System.Windows.Forms.Button();
            this.SupervisorsJSONRichTextBox = new System.Windows.Forms.RichTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.DatabaseDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StudentsDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // DatabaseDataGridView
            // 
            this.DatabaseDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DatabaseDataGridView.Location = new System.Drawing.Point(373, 50);
            this.DatabaseDataGridView.Name = "DatabaseDataGridView";
            this.DatabaseDataGridView.RowHeadersWidth = 62;
            this.DatabaseDataGridView.RowTemplate.Height = 28;
            this.DatabaseDataGridView.Size = new System.Drawing.Size(165, 258);
            this.DatabaseDataGridView.TabIndex = 0;
            this.DatabaseDataGridView.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DatabaseDataGridView_CellClick);
            // 
            // NameTextBox
            // 
            this.NameTextBox.Location = new System.Drawing.Point(66, 50);
            this.NameTextBox.Name = "NameTextBox";
            this.NameTextBox.Size = new System.Drawing.Size(140, 26);
            this.NameTextBox.TabIndex = 1;
            // 
            // RefreshButton
            // 
            this.RefreshButton.Location = new System.Drawing.Point(491, 322);
            this.RefreshButton.Name = "RefreshButton";
            this.RefreshButton.Size = new System.Drawing.Size(120, 50);
            this.RefreshButton.TabIndex = 5;
            this.RefreshButton.Text = "Refresh";
            this.RefreshButton.UseVisualStyleBackColor = true;
            this.RefreshButton.Click += new System.EventHandler(this.RefreshButton_Click);
            // 
            // DeleteButton
            // 
            this.DeleteButton.Location = new System.Drawing.Point(697, 329);
            this.DeleteButton.Name = "DeleteButton";
            this.DeleteButton.Size = new System.Drawing.Size(81, 37);
            this.DeleteButton.TabIndex = 6;
            this.DeleteButton.Text = "Delete";
            this.DeleteButton.UseVisualStyleBackColor = true;
            this.DeleteButton.Click += new System.EventHandler(this.DeleteButton_Click);
            // 
            // AddButton
            // 
            this.AddButton.Location = new System.Drawing.Point(131, 96);
            this.AddButton.Name = "AddButton";
            this.AddButton.Size = new System.Drawing.Size(75, 47);
            this.AddButton.TabIndex = 7;
            this.AddButton.Text = "Add";
            this.AddButton.UseVisualStyleBackColor = true;
            this.AddButton.Click += new System.EventHandler(this.AddButton_Click);
            // 
            // NameLabel
            // 
            this.NameLabel.AutoSize = true;
            this.NameLabel.Location = new System.Drawing.Point(3, 55);
            this.NameLabel.Name = "NameLabel";
            this.NameLabel.Size = new System.Drawing.Size(51, 20);
            this.NameLabel.TabIndex = 9;
            this.NameLabel.Text = "Name";
            // 
            // SupervisorNamesListBox
            // 
            this.SupervisorNamesListBox.FormattingEnabled = true;
            this.SupervisorNamesListBox.ItemHeight = 20;
            this.SupervisorNamesListBox.Location = new System.Drawing.Point(227, 50);
            this.SupervisorNamesListBox.Name = "SupervisorNamesListBox";
            this.SupervisorNamesListBox.Size = new System.Drawing.Size(125, 124);
            this.SupervisorNamesListBox.TabIndex = 13;
            // 
            // CreateStudentLabel
            // 
            this.CreateStudentLabel.AutoSize = true;
            this.CreateStudentLabel.Location = new System.Drawing.Point(48, 9);
            this.CreateStudentLabel.Name = "CreateStudentLabel";
            this.CreateStudentLabel.Size = new System.Drawing.Size(118, 20);
            this.CreateStudentLabel.TabIndex = 14;
            this.CreateStudentLabel.Text = "Create Student";
            // 
            // SupervisorsInfoLabel
            // 
            this.SupervisorsInfoLabel.AutoSize = true;
            this.SupervisorsInfoLabel.Location = new System.Drawing.Point(369, 24);
            this.SupervisorsInfoLabel.Name = "SupervisorsInfoLabel";
            this.SupervisorsInfoLabel.Size = new System.Drawing.Size(308, 20);
            this.SupervisorsInfoLabel.TabIndex = 15;
            this.SupervisorsInfoLabel.Text = "Select a supervisor to be added to student";
            // 
            // SupervisorNamesLabel
            // 
            this.SupervisorNamesLabel.AutoSize = true;
            this.SupervisorNamesLabel.Location = new System.Drawing.Point(227, 24);
            this.SupervisorNamesLabel.Name = "SupervisorNamesLabel";
            this.SupervisorNamesLabel.Size = new System.Drawing.Size(136, 20);
            this.SupervisorNamesLabel.TabIndex = 16;
            this.SupervisorNamesLabel.Text = "Supervisor names";
            // 
            // StudentsDataGridView
            // 
            this.StudentsDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.StudentsDataGridView.Location = new System.Drawing.Point(564, 50);
            this.StudentsDataGridView.Name = "StudentsDataGridView";
            this.StudentsDataGridView.RowHeadersWidth = 62;
            this.StudentsDataGridView.RowTemplate.Height = 28;
            this.StudentsDataGridView.Size = new System.Drawing.Size(183, 258);
            this.StudentsDataGridView.TabIndex = 17;
            this.StudentsDataGridView.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.StudentsDataGridView_CellClick);
            // 
            // StudentsLabel
            // 
            this.StudentsLabel.AutoSize = true;
            this.StudentsLabel.Location = new System.Drawing.Point(673, 27);
            this.StudentsLabel.Name = "StudentsLabel";
            this.StudentsLabel.Size = new System.Drawing.Size(74, 20);
            this.StudentsLabel.TabIndex = 18;
            this.StudentsLabel.Text = "Students";
            // 
            // SerializeToJSONButton
            // 
            this.SerializeToJSONButton.Location = new System.Drawing.Point(227, 342);
            this.SerializeToJSONButton.Name = "SerializeToJSONButton";
            this.SerializeToJSONButton.Size = new System.Drawing.Size(177, 52);
            this.SerializeToJSONButton.TabIndex = 19;
            this.SerializeToJSONButton.Text = "SerializeToJSON";
            this.SerializeToJSONButton.UseVisualStyleBackColor = true;
            this.SerializeToJSONButton.Click += new System.EventHandler(this.SerializeToJSONButton_Click);
            // 
            // SupervisorsJSONRichTextBox
            // 
            this.SupervisorsJSONRichTextBox.Location = new System.Drawing.Point(22, 180);
            this.SupervisorsJSONRichTextBox.Name = "SupervisorsJSONRichTextBox";
            this.SupervisorsJSONRichTextBox.Size = new System.Drawing.Size(330, 156);
            this.SupervisorsJSONRichTextBox.TabIndex = 20;
            this.SupervisorsJSONRichTextBox.Text = "";
            this.SupervisorsJSONRichTextBox.Visible = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.SupervisorsJSONRichTextBox);
            this.Controls.Add(this.SerializeToJSONButton);
            this.Controls.Add(this.StudentsLabel);
            this.Controls.Add(this.StudentsDataGridView);
            this.Controls.Add(this.SupervisorNamesLabel);
            this.Controls.Add(this.SupervisorsInfoLabel);
            this.Controls.Add(this.CreateStudentLabel);
            this.Controls.Add(this.SupervisorNamesListBox);
            this.Controls.Add(this.NameLabel);
            this.Controls.Add(this.AddButton);
            this.Controls.Add(this.DeleteButton);
            this.Controls.Add(this.RefreshButton);
            this.Controls.Add(this.NameTextBox);
            this.Controls.Add(this.DatabaseDataGridView);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.DatabaseDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StudentsDataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView DatabaseDataGridView;
        private System.Windows.Forms.TextBox NameTextBox;
        private System.Windows.Forms.Button RefreshButton;
        private System.Windows.Forms.Button DeleteButton;
        private System.Windows.Forms.Button AddButton;
        private System.Windows.Forms.Label NameLabel;
        private System.Windows.Forms.ListBox SupervisorNamesListBox;
        private System.Windows.Forms.Label CreateStudentLabel;
        private System.Windows.Forms.Label SupervisorsInfoLabel;
        private System.Windows.Forms.Label SupervisorNamesLabel;
        private System.Windows.Forms.DataGridView StudentsDataGridView;
        private System.Windows.Forms.Label StudentsLabel;
        private System.Windows.Forms.Button SerializeToJSONButton;
        private System.Windows.Forms.RichTextBox SupervisorsJSONRichTextBox;
    }
}

