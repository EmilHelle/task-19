﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Task19.Model;
using Newtonsoft.Json;

namespace Task19
{
    public partial class Form1 : Form
    {
        private SchoolDBContext schoolDBContext;
        private int supervisorsSelectedCellKey;
        private int studentsSelectedCellKey;
        public Form1()
        {
            InitializeComponent();
            this.schoolDBContext = new SchoolDBContext();


            //supervisors
            List<Supervisor> supervisors = this.schoolDBContext.Supervisors.ToList();

            foreach (Supervisor supervisor in supervisors)
            {
                //add student.name to listbox item
                SupervisorNamesListBox.Items.Add(supervisor.Name);
            }

            this.refreshDatabaseDataGridView();
            this.refreshStudentsDataGridView();
        }

        private void refreshDatabaseDataGridView() {
            BindingSource myBindingSource = new
           BindingSource();
            DatabaseDataGridView.DataSource = myBindingSource;
            myBindingSource.DataSource =
            this.schoolDBContext.Supervisors.ToList();

            DatabaseDataGridView.Refresh();
        }

        private void refreshStudentsDataGridView()
        {
            BindingSource myBindingSource = new
           BindingSource();
            StudentsDataGridView.DataSource = myBindingSource;
            myBindingSource.DataSource =
            this.schoolDBContext.Students.ToList();

            DatabaseDataGridView.Refresh();
        }

        private void deleteStudent(int index) {
            Student studentToBeDeleted =
             this.schoolDBContext.Students.Find(index);
            //Remove it
            this.schoolDBContext.Students.Remove(studentToBeDeleted);
            this.schoolDBContext.SaveChanges();
        }

        private void AddButton_Click(object sender, EventArgs e)
        {
            //add student to students database table
            Student student = new Model.Student
            {
                Name = NameTextBox.Text,
                SupervisorId = this.supervisorsSelectedCellKey
            };
            this.schoolDBContext.Students.Add(student);
            this.schoolDBContext.SaveChanges();
        }

        private void DatabaseDataGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            this.supervisorsSelectedCellKey = int.Parse(DatabaseDataGridView.Rows[DatabaseDataGridView.SelectedCells[0].RowIndex].Cells[0].Value.ToString());
        }

        private void StudentsDataGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            this.studentsSelectedCellKey = int.Parse(StudentsDataGridView.Rows[StudentsDataGridView.SelectedCells[0].RowIndex].Cells[0].Value.ToString());
        }

        private void DeleteButton_Click(object sender, EventArgs e)
        {
            this.deleteStudent(this.studentsSelectedCellKey);
        }

        private void RefreshButton_Click(object sender, EventArgs e)
        {
            this.refreshDatabaseDataGridView();
            this.refreshStudentsDataGridView();
        }

        private void SerializeToJSONButton_Click(object sender, EventArgs e)
        {
            
            //get json of dbcontext.supervisors
            var json = JsonConvert.SerializeObject(this.schoolDBContext.Supervisors.ToList());
            //display json in textbox.text = josn;
            SupervisorsJSONRichTextBox.Visible = true;
            SupervisorsJSONRichTextBox.Text = json;
        }
    }
}
